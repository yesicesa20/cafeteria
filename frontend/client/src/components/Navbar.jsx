import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Carrito from "./Carrito";
import axiosInstance from "../utils/axiosInstance";
import { useLocation } from "react-router-dom";

import './Navbar.css'
import { Menu, MenuItem, MenuButton, SubMenu } from '@szhsin/react-menu';


export default function Navbar({ onLogout, userId, carrito, countProductos }) {
  const [user, setUser] = useState();
  const location = useLocation();
  useEffect(() => {
    const handleUser = async () => {
      const response = await axiosInstance.get(`usuarios/${userId}/`);
      if (response.status === 200) {
        setUser(response.data);
      }
    };
    handleUser();
  }, [userId]);

  const logoutHandler = () => {
    onLogout();
  };
  const role = user ? user.group_name : "";

  return (
    

    <header className="header">
      {user && (
        <nav className="navbar">
          <ul>
             <h2 className="navbar-h2">
              CofeeTimeSystem!
             </h2>
          </ul>
          <ul  className="navbar-ul">
            
            <div className="flex gap-10">
              {role == "recepcionista" || role == "admin" ? (
                <li>
                  <Link to={"/productos"}>
                    <p
                      className={
                        location.pathname === "/productos"
                          ? "text-white underline underline-offset-8"
                          : "text-slate-200"
                      }
                    >
                      Productos
                    </p>
                  </Link>
                </li>
              ) : (
                ""
              )}
              {role === "admin" && (
                <li>
                  <Link to={"/usuarios"}>
                    <p
                      className={
                        location.pathname === "/usuarios"
                          ? "text-white underline underline-offset-8"
                          : "text-slate-200"
                      }
                    >
                      Usuarios
                    </p>
                  </Link>
                </li>
              )}
              {role == "recepcionista" || role == "cocinero" ? (
                <li>
                  <Link to={"/pedidos"}>
                    <p
                      className={
                        location.pathname === "/pedidos"
                          ? "text-white underline underline-offset-8"
                          : "text-slate-200"
                      }
                    >
                      Pedidos
                    </p>
                  </Link>
                </li>
              ) : (
                ""
              )}
            </div>
            <div className="flex gap-5">
              {role === "recepcionista" && (
                <Carrito carrito={carrito} countProductos={countProductos} />
              )}
              <li>
              <Menu menuButton={<MenuButton ><i className="fa fa-user-o"></i>{user.first_name + " " + user.last_name}</MenuButton>}>
                <MenuItem><button
                  className="text-white bg-stone-500 hover:bg-stone-600 dark:bg-blue-400"
                  onClick={logoutHandler}
                >
                  Salir
                </button></MenuItem>
                </Menu>
              </li>
              

            </div>
          </ul>
        </nav>
      )}
    </header>
  );
}
Navbar.propTypes = {
  onLogout: PropTypes.func.isRequired,
  userId: PropTypes.number,
};
