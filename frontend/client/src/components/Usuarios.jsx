import { useState, useEffect } from "react";
import userIcon from "../assets/user.svg";
import axiosInstance from "../utils/axiosInstance";
import editBtn from "../assets/edit-btn.svg";
import { useNavigate } from "react-router-dom";

const Usuarios = () => {
  const [usuarios, setUsuarios] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    let getUsuarios = async () => {
      let response = await axiosInstance.get("usuarios/");
      if (response.status === 200) {
        setUsuarios(response.data);
      }
    };
    getUsuarios();
  }, []);
  return (
    <div className="px-5 py-5 dark:bg-slate-800 h-full">
      <h1 className="text-center sm:text-center text-4xl dark:text-white">
        Lista de Usuarios
      </h1>
      <br></br>
      <ul className="grid lg:grid-cols-4 gap-4 grid-cols-2 md:grid-cols-3 place-content-center">
        {usuarios.map((usuario) => {
          return (
            usuario.group_name !== "admin" && (
              <li
                key={usuario.id}
                className="max-w-sm rounded overflow-hidden shadow-lg px-4 py-8"
              >
                <h3 className="text-center sm:text-center"> {usuario.first_name} {usuario.last_name}</h3>
                <br></br>
                <p className="dark:text-white text-red-300 text-base ">
                  Username:{" "}
                  <span className="text-gray-600 dark:text-white">
                    {usuario.username}{" "}
                  </span>
                </p>
                <p className="dark:text-white text-red-300 text-base">
                  Role:{" "}
                  <span className="text-gray-600 dark:text-white">
                    {usuario.group_name}
                  </span>
                </p>

                <div className="flex justify-end">

                <img
                    src={editBtn}
                    alt="Boton de editar producto"
                    className="w-6 h-6 hover:cursor-pointer mx-6"
                    onClick={() => {
                      navigate(`/usuario/${usuario.id}`);
                    }}
                  />

                </div>
              </li>
            )
          );
        })}
      </ul>
    </div>
  );
};

export default Usuarios;
