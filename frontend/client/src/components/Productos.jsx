/* eslint-disable react/prop-types */

import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { addPeriod } from "../utils/addPeriod.js";
import PropTypes from "prop-types";
import axiosInstance from "../utils/axiosInstance.js";
import editBtn from "../assets/edit-btn.svg";
import deleteBtn from "../assets/delete.svg";
import cartBtn from "../assets/cart.svg";
import './Navbar.css'
const Productos = (props) => {
  const [productos, setProductos] = useState([]);
  const { rol, carrito, setCarrito } = props;

  const [mostrarMensaje, setMostrarMensaje] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    getProductos();
  }, []);
  let getProductos = async () => {
    let response = await axiosInstance.get("productos/");
    if (response.status === 200) {
      setProductos(response.data);
      console.log(productos);
    }
  };
  async function eliminarProducto(id) {
    const response = await axiosInstance.delete(`productos/${id}/`);
    console.log(response);
    if (response.status === 204) {
      setProductos(productos.filter((producto) => producto.id !== id));
    } else {
      console.error("Error al eliminar");
    }
  }

  useEffect(() => {
    localStorage.setItem("productos", JSON.stringify(carrito.productos));
    localStorage.setItem("total", JSON.stringify(carrito.total));
    console.log(props);
  }, [carrito, props]);

  const handleLocalStorage = (data) => {
    // para mostrar mensaje de validacion
    setMostrarMensaje(true);
    setTimeout(() => {
      setMostrarMensaje(false);
    }, 1200);
    // agregar al carrito
    let carritoProducto = carrito.productos;
    carritoProducto.push(data.descripcion.toUpperCase());
    setCarrito((prevState) => ({
      productos: carritoProducto,
      total: prevState.total + data.precio,
    }));
  };
  return (
    <div className="px-5 py-5 dark:bg-slate-800 dark:text-white h-full">
      {mostrarMensaje && (
        <div className="w-full relative relative">
          <button className="absolute right-0 my-2 mx-5 bg-green-800 text-slate-100 animate-blunded">
            Se ha agregado al carrito ✅
          </button>
        </div>
      )}
      
        <h1 className="text-center sm:text-center text-4xl">Productos</h1>
        <div className="flex justify-end inline-block">
        {rol === "admin" && (
          <Link to="/producto/agregar/">
            <button className="my-10 bg-slate-300 hover:bg-zinc-500 dark:bg-black dark:hover:bg-black text-white font-bold py-2 px-4 rounded">
              Agregar
            </button>
          </Link>
        )}
      </div>

      <ul className="grid lg:grid-cols-4 gap-4 grid-cols-2 md:grid-cols-3 place-content-center">
        {productos.map((producto) => {
          return (
            <li
              key={producto.id}
              className="max-w-sm rounded overflow-hidden shadow-lg px-4 py-8 flex flex-col justify-between"
            >
              <div className="w-full flex justify-start">
                {rol === "admin" && (
                  <img
                    src={editBtn}
                    alt="Boton de editar producto"
                    className="w-6 h-6 hover:cursor-pointer"
                    onClick={() => {
                      navigate(`/producto/${producto.id}`);
                    }}
                  />
                )}
              </div>
              <div className="w-full flex justify-center">
                {producto.imagen && (
                  <img src={producto.imagen} className="w-[150px] h-25 my-5" />
                )}
                </div>
                
              <br></br>

              <p className="font-bold">{producto.descripcion.toUpperCase()}</p>
              <p>
                <strong>Precio:</strong>{" "}
                <span>{addPeriod(producto.precio)} G.s </span>{" "}
              </p>
              <div className="flex justify-end">
                {rol === "recepcionista" && (
                    <img
                  src={cartBtn}
                  alt="Agregar al carrito"
                  className="w-6 h-6 hover:cursor-pointer"
                  onClick={()=> handleLocalStorage(producto)}
                />

                )}
              </div>
              <div className="flex justify-end">
                {rol === "admin" && (
                  <img
                  src={deleteBtn}
                  alt="Eliminar"
                  className="w-6 h-6 hover:cursor-pointer"
                  onClick={()=> eliminarProducto(producto.id)}
                />
                )}
              </div>
            </li>
          );
        })}
      </ul>

     
    </div>
  );
};

export default Productos;
Productos.propTypes = {
  group: PropTypes.string,
};
