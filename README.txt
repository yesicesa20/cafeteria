
*Backend*

Acceder de la siguiente manera: 

(venv) PS C:\Users\yesic\Desktop\Cafeteria> cd .\backend\
(venv) PS C:\Users\yesic\Desktop\Cafeteria\backend>  cd .\cafeteria

Luego levantar el back de la siguiente manera: python .\manage.py runserver


*FrontEnd*

Acceder de la siguiente manera: 

(venv) PS C:\Users\yesic\Desktop\Cafeteria> cd .\frontend\
(venv) PS C:\Users\yesic\Desktop\Cafeteria\frontend> cd .\client\

Luego levantar el front de la siguiente manera: npm run dev